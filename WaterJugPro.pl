state(X,Y):-(X=:=0,Y=:=0,nl,write('X = 0, Y = 3 : Fill 3L jug'), state(X,3));
            (X=:=0,Y=:=0,nl,write('X = 4, Y = 0 : Fill 4L jug'), state(4,Y));
            (X=:=2,Y=:=0,nl,write('X = 0, Y = 2 : Goal !'));
            (X=:=4,Y=:=0,nl,write('X = 4, Y = 3 : Pour from 4L and 3L jug'), state(4,3));
            (X=:=0,Y=:=3,nl,write('X = 3, Y = 0 : Pour from 3L to 4L jug'), state(3,0));
            (X=:=3,Y=:=3,nl,write('X = 4, Y = 2 : Empty 3L jug'), state(4,2));
            (X=:=3,Y=:=0,nl,write('X = 3, Y = 3 : Fill 3L jug'), state(3,3));
