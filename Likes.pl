%% Demo coming from http://clwww.essex.ac.uk/course/LG519/2-facts/index_18.html
%%
%% Please load this file into SWI-Prolog
%%
%% Sam's likes and dislikes in food
%%
%% Considering the following will give some practice
%% in thinking about backtracking.
%%
%% You can also run this demo online at
%% http://swish.swi-prolog.org/?code=https://github.com/SWI-Prolog/swipl-devel/raw/master/demo/likes.pl&q=likes(sam,Food).

/** <examples>
?- likes(sam,dahl).
?- likes(sam,chop_suey).
?- likes(sam,pizza).
?- likes(sam,chips).
?- likes(sam,curry).
*/
%% san ชอบ Food ถ้า Food เป็นอาหาร indian และ Food อ่อน
likes(sam,Food) :-
    indian(Food),
    mild(Food).

%% san ชอบ Food ถ้า Food เป็นอาหาร chinese 
likes(sam,Food) :-
    chinese(Food).

%% san ชอบ Food ถ้า Food เป็นอาหาร italian 
likes(sam,Food) :-
    italian(Food).

